import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;

/* MusicCharts - CSVImport
 * provides functionality of importing data from csv
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 14.05.2021
 */
public class CsvImport {
	public static int lineNr = 0;

	public static void readSongsFromCsv(){
		
		try(BufferedReader buffer = Files.newBufferedReader(Application.PATH_TO_CSV)){
			String line = buffer.readLine();
			line = buffer.readLine();//to skip header line
			
			while(line != null) {
				String[] attributes = line.split(";");
				lineNr++;
				
				if(checkNumberOfCollums(attributes)) {
					new Song(attributes);
				}
				
				line = buffer.readLine();
			}
		}catch(IOException e){
			WriteOutput.appendToLog(e.getLocalizedMessage());
		}
	}
	
	private static boolean checkNumberOfCollums(String[] attributes) {
		boolean correctCountOfLines = true;
		
		if(attributes.length != Application.NUMBER_OF_COLLUMS) {
			WriteOutput.appendToLog("Mailformated Input @ line: ");
			for(int i=0;i<attributes.length;i++) {
				WriteOutput.appendToLog(attributes[i]);
				if(i<(attributes.length-1)) {
					WriteOutput.appendToLog(";");
				}
			}
			WriteOutput.appendToLog(" -- ignoring line\n");
			correctCountOfLines = false;
		}
		
		return correctCountOfLines;
	}
}
