import java.io.File;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* MusicCharts - Application
 * contains Mainclass for MusicChart Program
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 07.05.2021
 */
public class Application {
	public static final Path PATH_TO_CSV = Paths.get("src/musicSmall.csv"); //musicSmall.csv music2021
	public static final Path PATH_TO_EXPORT = Paths.get("src/musicSorted2021.csv");
	public static final Path PATH_TO_LOGFILE = Paths.get("src/log.log");
	public static final int NUMBER_OF_COLLUMS = 6;
	public static final char SEPERATOR = ';';
	
	public static void main(String[] args) {
		
		List <Artist> allArtists = new ArrayList<>();
		ReportCompare reportComp = new ReportCompare();
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		File logFile = new File(PATH_TO_LOGFILE.toString());
		if(logFile.exists()) {
			logFile.delete();
		}
		
		CsvImport.readSongsFromCsv();

		WriteOutput.writeToFreshCSV("Interpret"+SEPERATOR+"Number of Tracks"+SEPERATOR+" Average Rating"+SEPERATOR+" Single Ratings\n");
		
		allArtists = Artist.getAllArtists();
		Collections.sort(allArtists,reportComp);
		
		for(Artist singleArtist : allArtists) {
			
			String toCsv = singleArtist.name+SEPERATOR+singleArtist.numberOfSongs+SEPERATOR+
					df.format(singleArtist.getAverageRating())+SEPERATOR+singleArtist.singleRatings+"\n";
			WriteOutput.appendToCSV(toCsv.replace('[', ' ').replace(']', ' '));
		}
		
		System.out.println("Export done!"); //2BDeleted
	}
}
