import java.util.HashSet;
import java.util.Set;


/* MusicCharts - Song
 * Object Class for Song
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 14.05.2021
 */
public class Song {
	int thisWeek = 0;
	int lastWeek = 0;
	int weekInCharts = 0;
	String title = null;
	Set<Artist> artists = new HashSet<Artist>();
	float rating = Float.NaN;
	
	Song(String[] attributes){
		this.thisWeek = parseInt(attributes[0],"DW");
		this.weekInCharts = parseInt(attributes[2],"WW");
		if(attributes[1].contentEquals("")) {
			this.lastWeek = 0;
		}else {
			this.lastWeek = parseInt(attributes[1],"LW");
		}
		this.rating = parseFloat(attributes[5].replace(",", "."));
		this.title = attributes[3];
		this.artists = Artist.getArtistFromString(attributes[4],this.rating);
	}
	
	private static int parseInt(String attribute, String collumName) {
		int returnValue = Integer.MIN_VALUE;
		
		try {
			 returnValue = Integer.parseInt(attribute);
				
		}catch(NumberFormatException e) {
			WriteOutput.appendToLog("Number parsing error for "+collumName+" @ line: "+CsvImport.lineNr+" -- ignoring\n");
			WriteOutput.appendToLog(e.getLocalizedMessage()+"\n");
		}
		
		return returnValue;
	}
	
	private static float parseFloat(String attribute) {
		float returnValue = 0f;
		
		try {
			 returnValue = Float.parseFloat(attribute);
				
		}catch(NumberFormatException e) {
			WriteOutput.appendToLog("Number parsing error for Bewertung @ line: "+CsvImport.lineNr+" -- ignoring\n");
			WriteOutput.appendToLog(e.getLocalizedMessage()+"\n");
		}
		
		return returnValue;
	}
	
}
