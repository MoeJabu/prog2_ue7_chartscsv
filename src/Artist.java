import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* MusicCharts - Song
 * Object Class for Artists
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 14.05.2021
 */
public class Artist {
	private static HashMap <String,Artist> hashMappOfAllArtists = new HashMap<String,Artist>();
	
	String name = null;
	int numberOfSongs = 0;
	List<Float> singleRatings = new ArrayList<Float>();
	
	Artist(String name){
		this.name = name;
		incrementNumberOfSongs();
	}
	
	//static methods
	public static Set<Artist> getArtistFromString(String rawString, float rating){
		Set<Artist> artists= new HashSet<Artist>();
		
		String[] artistsOfSong = rawString.split(",");
		
		for(String singleArtistOfSong: artistsOfSong) {
			Artist artist = null;
			
			artist = addArtistToListIfNotExistingOrIncementNumberOfSongs(singleArtistOfSong.trim());
			artist.addARating(rating);
			artists.add(artist);				
		}	
		return artists;
	}
	
	private static Artist addArtistToListIfNotExistingOrIncementNumberOfSongs(String singleArtistOfSong) {
		Artist artist = null;
		
		if(!hashMappOfAllArtists.containsKey(singleArtistOfSong)) {
			artist = new Artist(singleArtistOfSong);
			hashMappOfAllArtists.put(singleArtistOfSong, artist);
		}else {
			artist = hashMappOfAllArtists.get(singleArtistOfSong);
			artist.incrementNumberOfSongs();
		}
		
		return artist;
	}
	
	public static List<Artist> getAllArtists() {
		ArrayList <Artist> returnList = new ArrayList <Artist>(hashMappOfAllArtists.values());
		return returnList;
	}
	
	//object methods
	public void addARating(float ratingToAdd) {
		if(ratingToAdd>0) {
			this.singleRatings.add(ratingToAdd);
		}
	}
	
	public float calculateSumOfRatings(){
		float sumOfRating = 0f;
		
		for(float rating:this.singleRatings) {
			sumOfRating += rating;
		}
		
		return sumOfRating;
	}
	
	public float getAverageRating() {
		return calculateSumOfRatings()/this.singleRatings.size();
	}	

	private void incrementNumberOfSongs() {
		this.numberOfSongs++;
	}

}
