import java.util.Comparator;

/*
* MusicCharts - ReportCompare
* provides a comperator Object for the csv export
* Author: Philip Moser
* Mail: philip.moser@edu.fh-joanneum.at
* Last-Change: 07.05.2021
*/
public class ReportCompare implements Comparator<Artist>{

	@Override
	public int compare(final Artist artist1,final Artist artist2) {
		int comp;
		comp = artist2.numberOfSongs - artist1.numberOfSongs;
		
		if (comp == 0) {
			comp = (int) (artist2.getAverageRating() - artist1.getAverageRating());
		}
				
		return comp;
	}

}
