import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

/* MusicCharts - CSVExport
 * provides functionality of exporting data to csv
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 07.05.2021
 */
public class WriteOutput {
	
	public static void appendToCSV(String text) {
		try(BufferedWriter writer = Files.newBufferedWriter(Application.PATH_TO_EXPORT, StandardCharsets.UTF_8, StandardOpenOption.APPEND, 
			StandardOpenOption.CREATE)){
			writer.write(text);
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	public static void writeToFreshCSV(String text) {
		try(BufferedWriter writer = Files.newBufferedWriter(Application.PATH_TO_EXPORT, StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING)){
			writer.write(text);
		}catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	public static void appendToLog(String text) {
		try(BufferedWriter writer = Files.newBufferedWriter(Application.PATH_TO_LOGFILE, StandardCharsets.UTF_8, StandardOpenOption.APPEND, 
			StandardOpenOption.CREATE)){
			writer.write(text);
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
}
